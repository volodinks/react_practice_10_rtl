import {useState} from 'react';

const DELAY = 500;
const DEFAULT_VALUE = 100;

export const useCounter = (num: number) => {
    const [counter, setCounter] = useState(num);
    const [value, setValue] = useState(DEFAULT_VALUE);
    const [loading, setLoading] = useState(false);

    const changeCounter = (num: number) => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            setCounter(counter + num);
        }, DELAY);
    };

    const incCounter = () => {
        changeCounter(1);
    };

    const decCounter = () => {
        changeCounter(-1);
    };

    return {
        counter,
        value,
        setValue,
        setCounter,
        incCounter,
        decCounter,
        loading
    };
};
