import './App.css';
import {Counter} from './components/counter';
import {MaterialUiCounter} from './components/mui-counter';

function App() {
  return (
      <div className="app">
        <Counter/>
        <hr/>
        <MaterialUiCounter/>
      </div>
  );
}

export default App;
