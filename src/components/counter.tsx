import React from 'react';
import {useCounter} from '../hooks/useCounter';
import './counter.css';

interface Props {
    count?: number
}

export const Counter = ({count = 0}: Props) => {
    const {
        counter,
        value,
        setValue,
        setCounter,
        incCounter,
        decCounter,
        loading
    } = useCounter(count);

    return (
        <div className="counter">
            {
                loading
                    ? <h3 data-testid="loading">Loading...</h3>
                    : <h3>{counter}</h3>
            }
            <button onClick={() => incCounter()} disabled={loading}>+</button>
            <button onClick={() => decCounter()} disabled={loading}>-</button>
            <div>
                <label htmlFor="set-counter">Counter value:</label>
                <input
                    type="number"
                    id="set-counter"
                    value={value}
                    onChange={(event) => setValue(Number(event.target.value))}
                />
                <button onClick={() => setCounter(value)} disabled={loading}>Set value</button>
            </div>
        </div>
    );
};

