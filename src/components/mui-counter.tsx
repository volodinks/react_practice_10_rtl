import React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import {useCounter} from '../hooks/useCounter';
import './counter.css';

interface Props {
    count?: number
}

export const MaterialUiCounter = ({count = 0}: Props) => {

    const {
        counter,
        value,
        setValue,
        setCounter,
        incCounter,
        decCounter,
        loading
    } = useCounter(count);

    return (
        <Box p={1}>
            <Box mb={2}>
                <TextField
                    id="set-counter"
                    label="Counter value:"
                    value={value}
                    onChange={(event) => setValue(Number(event.target.value))}
                />
                <Button
                    variant="contained"
                    onClick={() => setCounter(value)}
                    disabled={loading}>
                    Set value
                </Button>
            </Box>
            {loading
                ? <Box mb={4}><CircularProgress data-testid="loading"/></Box>
                : (
                    <Typography variant="h3" gutterBottom component="div">
                        {counter}
                    </Typography>
                )
            }
            <Button
                variant="contained"
                onClick={() => incCounter()}
                disabled={loading}>
                +
            </Button>
            <Button
                variant="contained"
                onClick={() => decCounter()}
                disabled={loading}>
                -
            </Button>
        </Box>
    );
};
